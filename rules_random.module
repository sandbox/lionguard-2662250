<?php
/**
 * Custom Rules Actions.
 */
function rules_random_rules_action_info() {

  $actions['random_int'] = array(
    'parameter' => array(
      'length' => array(
        'type' => 'integer',
        'label' => t('The number of digits you would like in the random number'),
        'default mode' => 'input',
      ),
    ),
    'provides' => array(
      'random_int' => array(
        'type' => 'integer',
        'label' => t('The random number'),
      ),
    ),
    'group' => t('Random'),
    'label' => t('Generate a random Integer'),
    'base' => 'rules_random_rules_action_random_int',
  );

  $actions['random_string'] = array(
    'parameter' => array(
      'length' => array(
        'type' => 'integer',
        'label' => t('The number of digits you would like in the random number'),
        'default mode' => 'input',
      ),
      'with_numbers' => array(
        'type' => 'boolean',
        'label' => t('Would you like numbers included in the generated string?'),
      ),
      'camel_case' => array(
        'type' => 'boolean',
        'label' => t('Would you like lowercase letters included in the generated string?'),
      ),
    ),
    'provides' => array(
      'random_string' => array(
        'type' => 'text',
        'label' => t('The random string'),
      ),
    ),
    'group' => t('Random'),
    'label' => t('Generate a random String'),
    'base' => 'rules_random_rules_action_random_string',
  );

  return $actions;
}

/**
 * Action: generate a random int of $length digits
 */
function rules_random_rules_action_random_int($length) {
    $result = '';

    for($i = 0; $i < $length; $i++) {
        $result .= mt_rand(0, 9);
    }

    return array(
      'random_int' => $result,
      );
}

/**
 * Action: generate a random string of $length digits
 * @length - number of digits
 * @letters_only - whether to omit numbers from the randomization
 * @camel_case - whether to include both upper and lowercase
 */
function rules_random_rules_action_random_string($length, $with_numbers = FALSE, $camel_case = FALSE) {
    $numbers = '0123456789';
    $character_set = ($with_numbers == TRUE) ? $numbers : '';
    $lowercase = 'abcdefghijklmnopqrstuvwxyz';
    $character_set = ($camel_case == TRUE) ? $character_set . $lowercase : $character_set;
    $character_set = $character_set . 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($character_set);
    $result = '';
    for ($i = 0; $i < $length; $i++) {
        $result .= $character_set[rand(0, $charactersLength - 1)];
    }

    return array(
      'random_string' => $result,
      );
}